cmake_minimum_required(VERSION 2.8)
project(simple_rostest)
add_compile_options(-std=c++11 -Wall -Wextra)

find_package(catkin REQUIRED COMPONENTS
  genmsg
  roscpp
  std_msgs
)

################################################
## Declare ROS messages, services and actions ##
################################################

add_service_files(
  DIRECTORY
  srv
  FILES
  AddTwoInts.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

###################################
## catkin specific configuration ##
###################################

catkin_package(
  CATKIN_DEPENDS
  std_msgs
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable(
  add_two_ints_service
  src/add_two_ints_service.cpp
)
target_link_libraries(
  add_two_ints_service
  ${catkin_LIBRARIES}
)
add_dependencies(
  add_two_ints_service
  ${simple_rostest_EXPORTED_TARGETS}
)

#############
## Install ##
#############

#############
## Testing ##
#############

if(CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)

  add_rostest_gtest(
  add_two_ints_service_client
  test/add_two_ints_service_client.launch
  test/add_two_ints_service_client.cpp
  )
  target_link_libraries(
  add_two_ints_service_client
  ${catkin_LIBRARIES}
  )
  add_dependencies(
  add_two_ints_service_client
  add_two_ints_service
  ${PROJECT_NAME}_generate_messages_cpp
  ${catkin_EXPORTED_TARGETS}
  )
endif()

